#include <iostream>

using namespace std;

int** crearMatriz(int, int);
void imprimirMatriz(int**, int, int);

int main() {
    int n, m;
    cout << "Ingrese el numero de filas: ";
    cin >> n;
    cout << "Ingrese el numero de columnas: ";
    cin >> m;

    int** matriz = crearMatriz(n, m);

    cout << "Matriz creada:" << "\n";
    imprimirMatriz(matriz, n, m);

    system("pause");
    delete[] matriz;

    return 0;
}

int** crearMatriz(int n, int m) {
    int** matriz = new int* [n];

    for (int i = 0; i < n; i++) {
        matriz[i] = new int[m];
        for (int j = 0; j < m; j++) {
            matriz[i][j] = 0;
        }

    }

    return matriz;
}

void imprimirMatriz(int** matriz, int n, int m) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cout << matriz[i][j] << " ";
        }
        cout << "\n";
    }
}
