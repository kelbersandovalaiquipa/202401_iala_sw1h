#include <iostream>
using namespace std;
void matris(int x, int y) {
	int** matris = new int*[x];
	for (int i = 0; i < x; ++i) {
		matris[i] = new int[y];
	}
	for (int j = 0; j < x; ++j) {
		for (int k = 0; k < y; ++k) {
			matris[j][k] = 0;
		}
	}
	for (int m = 0; m < x; ++m) {
		for (int n = 0; n < y; ++n) {
			cout << matris[m][n] << " ";
			if (n == y - 1) { cout << endl; }
		}
	}
}
int main() {
	int x, y;
	cout << "Ingrese el valor en x de la funcion: "; cin >> x;
	cout << endl << "Ingrese el valor en y de la funcion: "; cin >> y;
	matris(x, y);
	return 0;
}