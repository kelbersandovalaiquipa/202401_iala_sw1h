#include <iostream>
#include <random>

using namespace std;

int main() {

	random_device ran;
	uniform_int_distribution<int> dis(100, 500);
	uniform_int_distribution<long long int> ben(1, 10000);

	int jose = dis(ran);

	long long int* clemencio = new long long int[jose];
	for (int i = 0; i < jose; ++i) {
		clemencio[i] = ben(ran);
	}
	for (int j = 0; j < jose; ++j) {
		cout << clemencio[j] << endl;
	}
	cout << "El tamano del arreglo es: " << jose << endl;
	system("pause");
	delete[] clemencio;
	return 0;
}