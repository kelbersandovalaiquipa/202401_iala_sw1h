#include "pch.h"
#include "iostream"

using namespace std;

int** Matriz(int fila, int columna) {

    int** matriz = new int* [fila];
    for (int i = 0; i < fila; ++i) {
        matriz[i] = new int[columna];
    }
    return matriz;
}

void Print(int** matriz, int fila, int columna) {
    for (int i = 0; i < fila; i++) {
        for (int j = 0; j < columna; j++) {
            matriz[i][j] = 0;
            cout << matriz[i][j] << " ";
        }
        cout << endl;
    }
}

int main() {

    int fila;
    int columna;

    cout << "Ingrese el numero de\n" << "Filas del arreglo bidimensional : ";
    cin >> fila;
    cout << "Columnas del arreglo bidimensional: ";
    cin >> columna;

    int** matriz = Matriz(fila, columna);
    cout << endl << "Matriz de " << fila << "x" << columna << ": " << endl << endl;
    Print(matriz, fila, columna);

    delete[] matriz;

    return EXIT_SUCCESS;

}